# Slackware-Xfce-Classic

This project based on [Slackware 15](https://docs.slackware.com/slackware:current) /  [Xfce 4.16](xfce.org) and the [libxfce4ui-nocsd Fork](https://github.com/Xfce-Classic/libxfce4ui-nocsd), it does not  adopting Client Side Decoration by default.


# libxfce4ui-nocsd ( _https://github.com/Xfce-Classic/libxfce4ui-nocsd_ )

This is a fork of libxfce4ui with the explicit goal of removing Client-Side Decorations (CSD).

Currently, functions introduced in somewhere between libxfce4ui 4.15 and 4.16 enable CSD by default for all applications that use the XfceTitledDialog class. This is a rather invasive UI change and, for some, is entirely unwanted. Since, upstream has indicated that they have no intention to allow users to turn CSD off, this fork exists to allow users to disable CSD in any applications that use libxfce4ui.

We implement all CSD-specific functionality as ABI-compatible shims that simply forward to the relevant GtkDialog methods. This allows upstream applications built against upstream libxfce4ui to function with no modifications.

IMPORTANT: libxfce4ui-nocsd will only remove CSDs for applications that use the XfceTitledDialog class. To remove CSDs from any applications that use GTK3 Dialogs, you need to set the DialogsUseHeader option to false:

$ xfconf-query -c xsettings -p /Gtk/DialogsUseHeader -s false
This can also be done via the GUI under Settings Editor > xsettings > Gtk.


# Installing

**Preparation**

Slackware 15 / current basic install 

http://slackware.uk/people/alien-current-iso/

If you have installed the standard selected packages, upgrading is non-trivial.
You will have to remove old Xfce packages.

`slackpkg remove xfce`


**Build instruction**

```
$ git clone https://gitlab.com/slackernetuk/slackware-xfce-classic.git
$ cd slackware-xfce-classic
$ ./build.sh
```

Add to /etc/slackpkg/blacklist file

`[0-9]+_snuk`

`xwmconfig` to setup Xfce as standard-session


enjoy





