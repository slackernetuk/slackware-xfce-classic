#!/bin/bash

for i in \
xdg-dbus-proxy \
libwpe \
wpebackend-fdo \
libseccomp \
gst-plugins-bad \
geoclue2 \
enchant2 \
bubblewrap \
libmanette \
webkit2gtk \
python-whither \
gnome-backgrounds \
lightdm-webkit2-greeter \
; do
cd ${i} || exit 1
./${i}.SlackBuild || exit 1
cd ..
done
