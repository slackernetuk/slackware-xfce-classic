#!/bin/bash

for i in \
gnome-autoar \
libseccomp \
gnome-desktop \
libhandy \
libportal \
libgrss \
tracker \
tracker-miners \
nautilus \
; do
cd ${i} || exit 1
./${i}.SlackBuild || exit 1
cd ..
done
