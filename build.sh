# Xfce Build Script
#!/bin/bash

for package in \
libopenraw \
libgusb \
gnome-common \
gcab \
appstream-glib \
bamf \
cogl \
clutter \
xfce4-dev-tools \
libxfce4util \
xfconf \
libxfce4ui \
exo \
garcon \
xfce4-panel \
thunar \
thunar-volman \
tumbler \
xfce4-appfinder \
xfce4-power-manager \
xfce4-settings \
xfdesktop \
xfwm4 \
xfce4-session \
xfce4-terminal \
xfce4-notifyd \
xfdashboard \
xfce4-pulseaudio-plugin \
vala-panel-appmenu \
xfce4-whiskermenu-plugin \
xfce4-screensaver \
xfce4-screenshooter \
xfce4-mailwatch-plugin \
xfce4-weather-plugin \
xfce4-panel-profiles \
ristretto \
mousepad \
Kvantum \
elementary-xfce \
Orchis-theme \
cantarell-fonts \
; do
cd $package || exit 1
./${package}.SlackBuild || exit 1
cd ..
done


